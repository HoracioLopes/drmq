<?php

namespace Drupal\drmq\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "drmq_notifications_block",
 *   admin_label = @Translation("Notifications"),
 * )
 */
class DRMQNotificationsBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $uid = \Drupal::currentUser()->id();
    $user = User::load($uid);
    $target = $user->get('mail')->value;

    // Get the queue config and send it to the data to the queue.
    $config = \Drupal::config('rabbitmq.config');
    $queues = $config->get('queues');
    foreach ($queues as $name => &$queue) {
      if (!empty($queue['routing_keys'])) {
        foreach ($queue['routing_keys'] as &$key) {
          $key = str_replace('amq_', 'amq.', $key);
        }
      }
    }

    $queue_name = 'notifications';
    $queue_factory = \Drupal::service('queue');
    $queue_factory->get('notifications');
    $queue = $queue_factory->get($queue_name);
    $queue->setRoutingKey("dmq.topic.notifications.{$target}");
    $notifications_count = $queue->numberOfItems();
    //kint($queue);
    //kint($notifications_count);
    /*

    kint($notifications_count);
    */
    return [
      '#title' => $this->t('Notifications (@notifications_count)', array('@notifications_count' => $notifications_count)),
      '#markup' => $target
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['drmq_notifications_block_settings'] = $form_state->getValue('drmq_notifications_block_settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
