<?php

namespace Drupal\drmq\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Contribute form.
 */
class ExampleForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drmq_example_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Send some text to the queue.'),
      '#required' => TRUE,
    ];

    $form['target'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Routing Key'),
    ];

    $form['show'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the data you want to send to the queue.
    $data = $form_state->getValue('text');
    $target = $form_state->getValue('target');

    $data = [
      'message' => $data,
      'routing_key' => $target,
    ];

    _drmq_send_notification($data, $target);

    // Send some feedback.
    drupal_set_message(
      $this->t('You sent the following data to queue @queue: @email', [
        '@queue' => $queue_name,
        '@email' => $form_state->getValue('text'),
      ])
    );
  }

}
