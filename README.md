Add to settings.php:

```
// Rabbit MQ settings.

$settings['rabbitmq_credentials'] = array(
  'host' => 'localhost',
  'port' => 5672,
  'username' => 'guest',
  'password' => 'guest',
  'vhost' => '/',
  'ssl' => FALSE,
);

$settings['queue_service_nmq'] = 'queue.drmq';
```
